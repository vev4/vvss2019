package vvir2220MV.repository.interfaces;


import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;

import java.util.List;

public interface EmployeeRepositoryInterface {
	
	boolean addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException;
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria(String criteria);

}
