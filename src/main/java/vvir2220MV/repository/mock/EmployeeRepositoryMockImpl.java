package vvir2220MV.repository.mock;


import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;
import vvir2220MV.validator.EmployeeValidator;

import java.util.ArrayList;
import java.util.List;

public class EmployeeRepositoryMockImpl implements EmployeeRepositoryInterface {

    private List<Employee> employeeList;
    private EmployeeValidator employeeValidator;

    public EmployeeRepositoryMockImpl() {

        employeeValidator = new EmployeeValidator();
        employeeList = new ArrayList<Employee>();

        Employee Ionel = new Employee("Ionel", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "2500");
        Employee Mihai = new Employee("Mihai", "Dumitrescu", "1234567890876", DidacticFunction.LECTURER, "200");
        Employee Ionela = new Employee("Ionela", "Ionescu", "1234567890876", DidacticFunction.LECTURER, "500");
        Employee Mihaela = new Employee("Mihaela", "Pacuraru", "1224567890876", DidacticFunction.ASISTENT, "500");
        Employee Vasile = new Employee("Vasile", "Georgescu", "1234567890876", DidacticFunction.TEACHER, "1000");
        Employee Marin = new Employee("Marin", "Puscas", "1234567890876", DidacticFunction.TEACHER, "2400");

        employeeList.add(Ionel);
        employeeList.add(Mihai);
        employeeList.add(Ionela);
        employeeList.add(Mihaela);
        employeeList.add(Vasile);
        employeeList.add(Marin);
    }

    @Override
    public boolean addEmployee(Employee employee) {
        if (employeeValidator.isValid(employee)) {
            employeeList.add(employee);
            return true;
        }
        return false;
    }

    @Override
    public void deleteEmployee(Employee employee) {
        // TODO Auto-generated method stub
    }

    @Override
    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException {
        int index = -1;
        for (int i = 0; i < this.employeeList.size(); i++) {
            if (this.employeeList.get(i).getCnp().equals(oldEmployee.getCnp())) {
                index = i;
                break;
            }
        }
        if (index != -1) {
            this.employeeList.set(index, newEmployee);
        } else {
            throw new EmployeeException("Employee not found.");
        }
        // TODO Auto-generated method stub
    }

    @Override
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    @Override
    public List<Employee> getEmployeeByCriteria(String criteria) {
        // TODO Auto-generated method stub
        return null;
    }
}
