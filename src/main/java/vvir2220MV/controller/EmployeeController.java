package vvir2220MV.controller;

import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class EmployeeController {
    private EmployeeRepositoryInterface employeeRepository;

    public EmployeeController(EmployeeRepositoryInterface employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public void addEmployee(Employee employee) {
        employeeRepository.addEmployee(employee);
    }

    public List<Employee> getEmployeesList() {
        return employeeRepository.getEmployeeList();
    }

    public void modifyEmployee(Employee oldEmployee, Employee newEmployee) throws EmployeeException {
        employeeRepository.modifyEmployee(oldEmployee, newEmployee);
    }

    public boolean modifyDidacticFunction(Employee employee, String didacticFunction) throws EmployeeException {
        DidacticFunction df;
        if (employee == null)
            throw new EmployeeException("Invalid employee.");
        try {
            df = DidacticFunction.valueOf(didacticFunction);
        } catch (IllegalArgumentException ex) {
            df = null;
        }
        if (df == null) {
            throw new EmployeeException("Invalid didactic function.");
        }
        employee.setFunction(df);
        List<Employee> employees = this.getEmployeesList();
        int i = 0;
        while (i < employees.size()) {
            if (employees.get(i).getCnp().equals(employee.getCnp())) {
                this.employeeRepository.modifyEmployee(employees.get(i), employee);
                return true;
            }
            i++;
        }
        throw new EmployeeException("Employee not found.");
    }

    public void deleteEmployee(Employee employee) {
        employeeRepository.deleteEmployee(employee);
    }

    public void displaySortedEmployees(List<Employee> employeeList) throws EmployeeException {
        if (employeeList == null) throw new EmployeeException("No list provided.");
        employeeList.sort(
                Comparator
                        .comparing(Employee::getSalary, (a, b) -> {
                            Double salA = Double.parseDouble(a);
                            Double salB = Double.parseDouble(b);
                            return salB.compareTo(salA);
                        })
                        .thenComparing(
                                (Employee a, Employee b) -> {
                                    Date dateA = new Date(Integer.parseInt(a.getCnp().substring(1, 3)), Integer.parseInt(a.getCnp().substring(3, 5)), Integer.parseInt(a.getCnp().substring(5, 7)));
                                    Date dateB = new Date(Integer.parseInt(b.getCnp().substring(1, 3)), Integer.parseInt(b.getCnp().substring(3, 5)), Integer.parseInt(b.getCnp().substring(5, 7)));

                                    return dateA.compareTo(dateB);
                                })
        );
        employeeList.forEach(e -> {
            System.out.println(e.toString());
        });
    }
}
