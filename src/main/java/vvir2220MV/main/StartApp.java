package vvir2220MV.main;


//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

import vvir2220MV.controller.EmployeeController;
import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;
import vvir2220MV.repository.mock.EmployeeRepositoryMockImpl;
import vvir2220MV.validator.EmployeeValidator;

public class StartApp {

    public static void main(String[] args) {

        EmployeeRepositoryInterface employeesRepository = new EmployeeRepositoryMockImpl();
        EmployeeController employeeController = new EmployeeController(employeesRepository);

        for (Employee _employee : employeeController.getEmployeesList())
            System.out.println(_employee.toString());
        System.out.println("-----------------------------------------");

        Employee employee = new Employee("FirstName", "LastName", "1234567894321", DidacticFunction.ASISTENT, "2500");
        employeeController.addEmployee(employee);

        for (Employee _employee : employeeController.getEmployeesList())
            System.out.println(_employee.toString());

        EmployeeValidator validator = new EmployeeValidator();
        System.out.println(validator.isValid(new Employee("FirstName", "LastName", "1234567894322", DidacticFunction.TEACHER, "3400")));
        try {
            employeeController.displaySortedEmployees(employeeController.getEmployeesList());
        } catch (EmployeeException e) {
            e.printStackTrace();
        }

    }

}
