package vvir2220MV;

import org.junit.Before;
import org.junit.Test;
import vvir2220MV.controller.EmployeeController;
import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;
import vvir2220MV.repository.mock.EmployeeRepositoryMockImpl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

public class Integration_BigBang {
    static EmployeeRepositoryInterface employeeRepository;
    static EmployeeController employeeController;

    @Before
    public void setUp() {
        employeeRepository = new EmployeeRepositoryMockImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    // F01
    @Test
    public void TC01_EC() {
        assertTrue(employeeRepository.addEmployee(new Employee("nume", "prenume", "1234567890123", DidacticFunction.ASISTENT, "1231")));
    }


    // F02
    @Test
    public void TC02() throws EmployeeException {
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        employeeController.addEmployee(e);
        assertTrue(employeeController.modifyDidacticFunction(e, DidacticFunction.TEACHER.toString()));
        assertEquals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1).getFunction(), DidacticFunction.TEACHER);
    }

    // F03
    @Test
    public void TC06() {
        // valid
        PrintStream oldOut = System.out;

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            employeeController.displaySortedEmployees(employeeController.getEmployeesList());
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
        String contentString = outContent.toString();
        String[] lines = contentString.split("\n");
        assertEquals(lines[0].split(";")[4].trim(), "2500".trim());
        assertEquals(lines[3].split(";")[0].trim(), "Mihaela".trim());
        System.setOut(oldOut);
    }

    @Test
    public void allModules() {
        // F01
        assertTrue(employeeRepository.addEmployee(new Employee("M", "prenume", "1231231231231", DidacticFunction.ASISTENT, "1231")));

        // F02
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        employeeController.addEmployee(e);
        try {
            employeeController.modifyDidacticFunction(e, "");
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "Invalid didactic function.");
        }

        // F03
        try {
            employeeController.displaySortedEmployees(null);
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "No list provided.");
        }
    }

}
