package vvir2220MV;

import org.junit.BeforeClass;
import org.junit.Test;
import vvir2220MV.controller.EmployeeController;
import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.exception.EmployeeException;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;
import vvir2220MV.repository.mock.EmployeeRepositoryMockImpl;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class F02_TCS {
    static EmployeeRepositoryInterface employeeRepository;
    static EmployeeController employeeController;

    @BeforeClass
    public static void setUp() {
        employeeRepository = new EmployeeRepositoryMockImpl();
        employeeController = new EmployeeController(employeeRepository);
    }

    @Test
    public void TC01() {
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        try {
            employeeController.modifyDidacticFunction(e, DidacticFunction.ASISTENT.toString());
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "Employee not found.");
        }
    }

    @Test
    public void TC02() throws EmployeeException {
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        employeeController.addEmployee(e);
        assertTrue(employeeController.modifyDidacticFunction(e, DidacticFunction.TEACHER.toString()));
        assertEquals(employeeRepository.getEmployeeList().get(employeeRepository.getEmployeeList().size() - 1).getFunction(), DidacticFunction.TEACHER);
    }

    @Test
    public void TC03() {
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        employeeController.addEmployee(e);
        try {
            employeeController.modifyDidacticFunction(e, "asd");
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "Invalid didactic function.");
        }
    }

    @Test
    public void TC04() {
        Employee e = new Employee("a", "a", "1234567890123", DidacticFunction.ASISTENT, "123");
        employeeController.addEmployee(e);
        try {
            employeeController.modifyDidacticFunction(null, DidacticFunction.ASISTENT.toString());
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "Invalid employee.");
        }
    }

    @Test
    public void TC05() {
        Employee e = new Employee("c", "d", "3234567890123", DidacticFunction.ASISTENT, "123");
        try {
            employeeController.modifyDidacticFunction(e, DidacticFunction.ASISTENT.toString());
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "Employee not found.");
        }
    }

    @Test
    public void TC06() {
        // valid
        PrintStream oldOut = System.out;

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outContent));

        try {
            employeeController.displaySortedEmployees(employeeController.getEmployeesList());
        } catch (EmployeeException e) {
            e.printStackTrace();
        }
        String contentString = outContent.toString();
        String[] lines = contentString.split("\n");
        assertEquals(lines[0].split(";")[4].trim(), "2500".trim());
        assertEquals(lines[3].split(";")[0].trim(), "Mihaela".trim());
        System.setOut(oldOut);
    }

    @Test
    public void TC07() {
        try {
            employeeController.displaySortedEmployees(null);
            fail();
        } catch (EmployeeException ex) {
            assertEquals(ex.getMessage(), "No list provided.");
        }
    }
}
