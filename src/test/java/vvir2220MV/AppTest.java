package vvir2220MV;

import org.junit.BeforeClass;
import org.junit.Test;
import vvir2220MV.enumeration.DidacticFunction;
import vvir2220MV.model.Employee;
import vvir2220MV.repository.interfaces.EmployeeRepositoryInterface;
import vvir2220MV.repository.mock.EmployeeRepositoryMockImpl;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {
    static EmployeeRepositoryInterface employeeRepositoryInterface;

    @BeforeClass
    public static void setUp() {
        employeeRepositoryInterface = new EmployeeRepositoryMockImpl();
    }

    @Test
    public void TC01_EC() {
        assertTrue(employeeRepositoryInterface.addEmployee(new Employee("nume", "prenume", "1234567890123", DidacticFunction.ASISTENT, "1231")));
    }

    @Test
    public void TC02_EC() {
        assertFalse(employeeRepositoryInterface.addEmployee(new Employee("", "prenume", "1234567890123", DidacticFunction.TEACHER, "12333")));
    }

    @Test
    public void TC03_EC() {
        assertTrue(employeeRepositoryInterface.addEmployee(new Employee("nume", "prenume", "1234567890123", DidacticFunction.LECTURER, "1231")));
    }

    @Test
    public void TC04_EC() {
        assertFalse(employeeRepositoryInterface.addEmployee(new Employee("nume", "", "1231231231233", DidacticFunction.TEACHER, "-1")));
    }

    @Test
    public void TC01_BVA(){
        assertFalse(employeeRepositoryInterface.addEmployee(new Employee("", "prenume", "1231231231231", DidacticFunction.ASISTENT, "1231")));
    }

    @Test
    public void TC03_BVA(){
        assertTrue(employeeRepositoryInterface.addEmployee(new Employee("M", "prenume", "1231231231231", DidacticFunction.ASISTENT, "1231")));
    }

    @Test
    public void TC04_BVA(){
        assertTrue(employeeRepositoryInterface.addEmployee(new Employee("nume", "D", "1231231231231", DidacticFunction.ASISTENT, "1231")));
    }
}
