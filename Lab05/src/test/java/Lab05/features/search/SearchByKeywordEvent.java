package Lab05.features.search;

import Lab05.steps.serenity.EndUserBileteSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

@RunWith(SerenityRunner.class)
public class SearchByKeywordEvent {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserBileteSteps user;

    @Issue("#BILETE-1")
    @Test
    public void searching_by_keyword_b_should_display_the_corresponding_event() throws InterruptedException {
        webdriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        user.is_home_page();
        user.looks_for("b");
        user.should_see_event("Concert Regal - Bruch, Rachmaninov - ONR");
    }

    @Test
    public void searching_by_a_displays_500() {
        user.is_home_page();
        user.looks_for("a");
        user.should_see_500();
    }
} 