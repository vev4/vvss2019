package Lab05.features.register;

import Lab05.steps.serenity.EndUserRegisterSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class RegisterEventInvalid {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserRegisterSteps user;

    @Issue("#BILETE-2")

    @Test
    public void register_with_invalid_data() {
        user.is_register_page();
        user.registers("", "abcdefgh!", "abcdefgh!");
        user.clicksRegister();
        user.should_see_email_warning();
    }
} 