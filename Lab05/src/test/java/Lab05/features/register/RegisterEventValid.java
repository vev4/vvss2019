package Lab05.features.register;

import Lab05.steps.serenity.EndUserRegisterSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class RegisterEventValid {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserRegisterSteps user;

    @Issue("#BILETE-2")
    @Test
    public void register_with_valid_data() throws InterruptedException {
        user.is_register_page();
        webdriver.manage().window().maximize();
        JavascriptExecutor jse = (JavascriptExecutor) webdriver;
        jse.executeScript("window.scrollBy(0,100)");
        user.registers(System.currentTimeMillis() + "@hotmail.com", "abcdefgh!", "abcdefgh!");
        user.clicksRegister();
        assert (webdriver.getCurrentUrl().equals("https://www.bilete.ro/"));
    }

} 