package Lab05.features.booking;

import Lab05.steps.serenity.EndUserBookingSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Issue;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class BookingEventValid {

    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @Steps
    public EndUserBookingSteps user;

    @Issue("#BILETE-3")
    @Test
    public void book_valid_number_of_tickets() {
        user.is_booking_page();
        JavascriptExecutor jse = (JavascriptExecutor) webdriver;
        jse.executeScript("window.scrollBy(0,100)");
        webdriver.manage().window().maximize();
        user.is_panel_expanded();
        user.choose_tickets();
        user.book();
        assert (webdriver.getCurrentUrl().equals("https://www.bilete.ro/mvc/checkout/index"));

    }
} 