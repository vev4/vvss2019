package Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.bilete.ro/cauta/")
public class ResultsPage extends PageObject {

    @FindBy(name = "q")
    private WebElementFacade searchField;

    @FindBy(className = "form-control searchbox searchbox-with-autocomplete")
    private WebElementFacade searchButton;

    public void enter_keywords(String keyword) {
        searchField.type(keyword);
    }

    public void search() {
        searchButton.click();
    }

    public List<String> getResults() {
        WebElementFacade resultList = find(By.xpath("/html/body/main/section[2]/div/div[1]/div[1]"));
        return resultList.findElements(By.className("ev-thumb-title")).stream()
                .map(element -> element.getText())
                .collect(Collectors.toList());
    }

    public String getFirstH1() {
        WebElementFacade h1 = find(By.tagName("h1"));
        return h1.getText();
    }
}