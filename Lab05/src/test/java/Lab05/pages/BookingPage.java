package Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://www.bilete.ro/the-mission-in-the-forest-2/the-mission-in-the-forest-2-22-iun-2019/")
public class BookingPage extends PageObject {

    @FindBy(xpath = "/html/body/main/section[2]/div/div/div/div[2]/div[1]/div[1]/div/div[1]")
    private WebElementFacade panel;

    @FindBy(xpath = "/html/body/main/section[2]/div/div/div/div[2]/div[1]/div[1]/div/div[1]/div/div/div/div/div[4]/form/button[2]")
    private WebElementFacade bookButton;

    @FindBy(xpath = "/html/body/main/section[2]/div/div/div/div[2]/div[1]/div[1]/div/div[1]/div/div/div/div/div[4]/form/button[1]")
    private WebElementFacade bookinvButton;

    @FindBy(xpath = "//*[@id=\"seat_count\"]")
    private WebElementFacade ticketSelect;

    public void expand_panel() {
        panel.click();
    }

    public void book() {
        bookButton.click();
    }

    public void selectTicket() {
        ticketSelect.selectByIndex(3);
    }


    public void bookInv() {
    }
}