package Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.bilete.ro/")
public class HomePage extends PageObject {

    @FindBy(xpath = "/html/body/nav/div[2]/div/div[5]/div/div/form/div/input")
    private WebElementFacade searchField;

    @FindBy(xpath = "/html/body/nav/div[2]/div/div[5]/div/div/form/div/button")
    private WebElementFacade searchButton;

    public void enter_keywords(String keyword) {
        searchField.click();
        searchField.type(keyword);
    }

    public void search() {
        searchButton.click();
    }

    public List<String> getDefinitions() {
        WebElementFacade definitionList = find(By.tagName("ol"));
        return definitionList.findElements(By.tagName("li")).stream()
                .map(element -> element.getText())
                .collect(Collectors.toList());
    }
}