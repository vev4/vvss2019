package Lab05.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;

import java.util.List;
import java.util.stream.Collectors;

@DefaultUrl("https://www.bilete.ro/mvc/Account/Register")
public class RegisterPage extends PageObject {

    @FindBy(id = "Email")
    private WebElementFacade emailField;
    @FindBy(id = "Password")
    private WebElementFacade passwordField;
    @FindBy(id = "ConfirmPassword")
    private WebElementFacade cfpasswordField;

    @FindBy(xpath = "/html/body/main/form/div[5]/div/input")
    private WebElementFacade registerButton;

    public void enter_email(String email) {
        emailField.click();
        emailField.type(email);
    }

    public void enter_pass(String pass) {
        passwordField.click();
        passwordField.type(pass);
    }

    public void enter_cfpass(String pass) {
        cfpasswordField.click();
        cfpasswordField.type(pass);
    }

    public void register() {
        registerButton.click();
    }

    public List<String> getWarnings() {
        WebElementFacade warningList = find(By.xpath("/html/body/main/form/div[1]/div/div/ul"));
        return warningList.findElements(By.tagName("li")).stream()
                .map(element -> element.getText())
                .collect(Collectors.toList());
    }
}