package Lab05.steps.serenity;

import Lab05.pages.HomePage;
import Lab05.pages.RegisterPage;
import Lab05.pages.ResultsPage;
import jnr.x86asm.Register;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserBileteSteps {

    HomePage homePage;
    ResultsPage resultsPage;
    RegisterPage registerPage;

    @Step
    public void enters(String keyword) {
        homePage.enter_keywords(keyword);
    }

    @Step
    public void starts_search() {
        homePage.search();
    }

    @Step
    public void should_see_event(String event) {
        assertThat(resultsPage.getResults(), hasItem(containsString(event)));
    }

    @Step
    public void is_home_page() {
        homePage.open();
    }


    @Step
    public void looks_for(String event) {
        enters(event);
        starts_search();
    }

    @Step
    public void should_see_500() {
        assertThat(resultsPage.getFirstH1(), containsString("500"));
    }
}