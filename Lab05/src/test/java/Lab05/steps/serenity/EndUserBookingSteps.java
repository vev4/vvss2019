package Lab05.steps.serenity;

import Lab05.pages.BookingPage;
import net.thucydides.core.annotations.Step;

public class EndUserBookingSteps {

    BookingPage page;

    @Step
    public void is_panel_expanded() {
        page.expand_panel();
    }

    @Step
    public void book() {
        page.book();
    }


    @Step
    public void is_booking_page() {
        page.open();
    }

    @Step
    public void choose_tickets() {
        page.selectTicket();
    }

    @Step
    public void bookInv() {
        page.bookInv();
    }
}