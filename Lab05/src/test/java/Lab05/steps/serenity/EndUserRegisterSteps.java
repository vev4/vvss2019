package Lab05.steps.serenity;

import Lab05.pages.RegisterPage;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;

public class EndUserRegisterSteps {
    RegisterPage registerPage;

    @Step
    public void entersEmail(String keyword) {
        registerPage.enter_email(keyword);
    }

    @Step
    public void entersPassword(String keyword) {
        registerPage.enter_pass(keyword);
    }

    @Step
    public void entersCfpassword(String keyword) {
        registerPage.enter_cfpass(keyword);
    }

    @Step
    public void clicksRegister() {
        registerPage.register();
    }

    @Step
    public void should_see_warning(String warning) {
        assertThat(registerPage.getWarnings(), hasItem(containsString(warning)));
    }

    @Step
    public void is_register_page() {
        registerPage.open();
    }

    @Step
    public void registers(String email, String password, String cfPassword) {
        entersPassword(password);
        entersCfpassword(cfPassword);
        entersEmail(email);
    }

    @Step
    public void should_see_email_warning() {
        assertThat(registerPage.getWarnings(), hasItem(containsString("Email este necesar")));
    }
}